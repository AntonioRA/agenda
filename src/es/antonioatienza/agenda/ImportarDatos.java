/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agenda;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author Antonio Rojo Atienza
 */
public class ImportarDatos {

    private static final Logger LOG = Logger.getLogger(ImportarDatos.class.getName());
    public static ArrayList listaPersonas;

    public static void importar() {
        String nombreFichero = "agenda.csv";
        //Declaramos una variable BufferedReader
        BufferedReader br = null;
        try {
            //Creamos un objeto BufferedReader al que se le pasa 
            //un objeto FileReader con el nombre del fichero
            br = new BufferedReader(new FileReader(nombreFichero));
            //Leer la primera línea, guardando en un String
            String texto = br.readLine();
            //Repetir mientras no se llegue al final del fichero
            listaPersonas = new ArrayList();
            LOG.fine(String.valueOf(listaPersonas.size()));
            while (texto != null) {
                //Hacer lo que sea con la línea leída
                String[] dato = texto.split(";");
                Persona persona = new Persona(0, dato[0], dato[1], dato[2], dato[3], dato[4], dato[5], dato[6], dato[7], dato[8]);
                //Añadimos al ArrayList cada una de las personas
                listaPersonas.add(persona);
                LOG.warning(dato[0] + " " + dato[1] + " " + dato[2].charAt(0));
                texto = br.readLine();
            }
            LOG.fine(String.valueOf(listaPersonas.size()));
        } catch (FileNotFoundException e) {
            LOG.finest("Error: Fichero no encontrado");
            LOG.finest(e.getMessage());
        } catch (Exception e) {
            LOG.finest("Error de lectura del fichero: faltan datos");
            LOG.finest(e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                LOG.finest("Error al cerrar el fichero");
                LOG.finest(e.getMessage());
            }
        }
    }
}