/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agenda;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Antonio Rojo Atienza
 */
public class Agenda extends Application {

    public static StackPane root;

    @Override
    public void start(Stage primaryStage) {
        try {
            root = new StackPane();
            // Cargar primera pantalla (pantallaInicial)
            Parent pantallaInicial = FXMLLoader.load(getClass().getResource("fxml/PantallaInicial.fxml"));
            root.getChildren().add(pantallaInicial);

            Scene scene = new Scene(root, 360, 360);
            primaryStage.setTitle("Agenda");
            primaryStage.getIcons().add(new Image("es/antonioatienza/agenda/imagenes/ic_contacts_black_24dp_2x.png"));
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
            // Cerrar la aplicación si no se ha podido cargar la pantalla
            Platform.exit();
        }
//        try {
//            // Crear objeto JAXB para interpretar objetos 'Personas' desde XML
//            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//            // Generar lista de objetos desde XML descargado de URL
//            URL url = new URL("http://antonioatienza.esy.es/personas.xml");
//            InputStream is = url.openStream();
//            personas = (Personas) jaxbUnmarshaller.unmarshal(is);
//        } catch (JAXBException | IOException ex) {
//            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}