/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agenda;

import static es.antonioatienza.agenda.Agenda.root;
//import java.io.BufferedWriter;
//import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * FXML Controller class
 *
 * @author Antonio Atienza
 */
public class PantallaInicialControlador implements Initializable {

    @FXML
    private Pane panel;
    public static Personas personas;
    private static final Logger LOG = Logger.getLogger(Agenda.class.getName());
    public static TableView<Persona> tablaPersonas = new TableView<Persona>();
    public static ObservableList<Persona> listaObservablePersonas;
    public static Persona personaSeleccionada;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        personas = new Personas();
        try {
            // Crear una conexión con la URL del Servlet
            String strConnection = "http://localhost:8084/AgendaWeb/AgendaServlet";
            URL url2 = new URL(strConnection);
            URLConnection uc = url2.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            // La conexión se va a realizar para poder enviar y recibir información
            // en formato XML
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "text/xml");
            // Se va a realizar una petición con el método GET
            conn.setRequestMethod("GET");

            // Ejecutar la conexión y obtener la respuesta
            InputStreamReader isr = new InputStreamReader(conn.getInputStream());

            // Procesar la respuesta (XML) y obtenerla como un objeto de tipo Personas
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Object response = jaxbUnmarshaller.unmarshal(isr);
            isr.close();

            // Convertir a la clase Persona el objeto obtenido en la respuesta
            personas = (Personas) response;

        } catch (JAXBException | IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }

//        try {
//            //Se indica el nombre de la clase que contiene la lista de objetos
//            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
//            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//            //Indicar que se desea generar el xml con saltos de línea y tabuladores
//            //para facilitar su lectura
//            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//            //Generar XML mostrándolo por la salida estándar
//            jaxbMarshaller.marshal(personas, System.out);
//            //Generar XML guardándolo en un archivo local
//            BufferedWriter bw = new BufferedWriter(new FileWriter("personas.xml"));
//            jaxbMarshaller.marshal(personas, bw);
//        } catch (JAXBException | IOException ex) {
//            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
//        }
        listaObservablePersonas = FXCollections.observableArrayList(personas.getListaPersona());

        TableColumn nombreColumna = new TableColumn("Nombre");
        nombreColumna.setMinWidth(150);
        nombreColumna.setCellValueFactory(new PropertyValueFactory<Persona, String>("nombre"));

        TableColumn apellidosColumna = new TableColumn("Apellidos");
        apellidosColumna.setMinWidth(200);
        apellidosColumna.setCellValueFactory(new PropertyValueFactory<Persona, String>("apellidos"));

        tablaPersonas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tablaPersonas.setItems(listaObservablePersonas);
        tablaPersonas.getColumns().addAll(nombreColumna, apellidosColumna);
        panel.getChildren().addAll(tablaPersonas);

        tablaPersonas.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                personaSeleccionada = tablaPersonas.getSelectionModel().getSelectedItem();
                mostrarPantalla();
                LOG.finest("Seleccionado el objeto Persona");
            }
        });
    }

    @FXML
    private void botonAñadir(MouseEvent event) {
        Persona persona = new Persona();
        tablaPersonas.getItems().add(persona);
        PantallaDatosControlador.personaNueva = persona;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/PantallaDatos.fxml"));
            Parent pantallaDatos = loader.load();
            PantallaDatosControlador pantallaDatosControlador = loader.getController();
            // Pasar una referencia a la tabla para que pantallaDatos pueda acceder a su contenido
            pantallaDatosControlador.setTipoPeticion(PantallaDatosControlador.PETICION_POST);
            pantallaDatosControlador.setTableView(tablaPersonas);
            pantallaDatosControlador.mostrarDatosPersonaNueva();
            root.getChildren().add(pantallaDatos);
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void mostrarPantalla() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/PantallaDatos.fxml"));
            Parent pantallaDatos = loader.load();
            PantallaDatosControlador pantallaDatosControlador = loader.getController();
            // Pasar una referencia a la tabla para que pantallaDatos pueda acceder a su contenido
            pantallaDatosControlador.setTipoPeticion(PantallaDatosControlador.PETICION_PUT);
            pantallaDatosControlador.setTableView(tablaPersonas);
            pantallaDatosControlador.mostrarDatosPersonaSeleccionada();
            root.getChildren().add(pantallaDatos);
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}