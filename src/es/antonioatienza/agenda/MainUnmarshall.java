/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agenda;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Antonio Atienza
 */
public class MainUnmarshall {

    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("personas.xml"));
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Personas personas = (Personas) jaxbUnmarshaller.unmarshal(br);
            for (Persona persona : personas.getListaPersona()) {
                System.out.println("id: " + persona.getId());
                System.out.println("nombre: " + persona.getNombre());
                System.out.println("apellidos: " + persona.getApellidos());
                System.out.println("sexo: " + persona.getSexo());
                System.out.println("dni: " + persona.getDni());
                System.out.println("direccion: " + persona.getDireccion());
                System.out.println("codigoPostal: " + persona.getCodigoPostal());
                System.out.println("poblacion: " + persona.getPoblacion());
                System.out.println("telefono: " + persona.getTelefono());
                System.out.println("movil: " + persona.getMovil());
                System.out.println("fecha: " + persona.getFecha());
                System.out.println();
            }
        } catch (JAXBException | FileNotFoundException ex) {
            Logger.getLogger(MainUnmarshall.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
