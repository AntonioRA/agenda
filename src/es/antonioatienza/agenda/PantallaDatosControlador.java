/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agenda;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * FXML Controller class
 *
 * @author Antonio Rojo Atienza
 */
public class PantallaDatosControlador implements Initializable {

    private static final Logger LOG = Logger.getLogger(PantallaDatosControlador.class.getName());

    @FXML
    private TextField textFieldNombre;
    @FXML
    private TextField textFieldSexo;
    @FXML
    private TextField textFieldApellidos;
    @FXML
    private TextField textFieldDni;
    @FXML
    private TextField textFieldDireccion;
    @FXML
    private TextField textFieldCP;
    @FXML
    private TextField textFieldPoblacion;
    @FXML
    private TextField textFieldTfnoFijo;
    @FXML
    private TextField textFieldTfnoMovil;
    @FXML
    private DatePicker datePickerFecha;

    private TableView<Persona> tableView;
    public static Persona personaNueva;
    private byte tipoPeticion;
    public static final byte PETICION_POST = 0;
    public static final byte PETICION_PUT = 1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void setTipoPeticion(byte tipoPeticion) {
        this.tipoPeticion = tipoPeticion;
    }

    public void setTableView(TableView<Persona> tableView) {
        this.tableView = tableView;
    }

    public void mostrarDatosPersonaSeleccionada() {
        // Obtener el objeto que está seleccionado en la tabla
        PantallaInicialControlador.personaSeleccionada = tableView.getSelectionModel().getSelectedItem();
        LOG.fine("mostrarDatosPersonaSeleccionada Nombre: " + PantallaInicialControlador.personaSeleccionada.getNombre());
        LOG.fine("mostrarDatosPersonaSeleccionada ID: " + PantallaInicialControlador.personaSeleccionada.getId());
        // Rellenar los controles de la pantalla con los datos del objeto seleccionado
        textFieldNombre.setText(PantallaInicialControlador.personaSeleccionada.getNombre());
        textFieldApellidos.setText(PantallaInicialControlador.personaSeleccionada.getApellidos());
        textFieldSexo.setText(PantallaInicialControlador.personaSeleccionada.getSexo());
        textFieldDni.setText(PantallaInicialControlador.personaSeleccionada.getDni());
        textFieldDireccion.setText(PantallaInicialControlador.personaSeleccionada.getDireccion());
        textFieldCP.setText(PantallaInicialControlador.personaSeleccionada.getCodigoPostal());
        textFieldPoblacion.setText(PantallaInicialControlador.personaSeleccionada.getPoblacion());
        LOG.warning(PantallaInicialControlador.personaSeleccionada.getPoblacion());
        textFieldTfnoFijo.setText(PantallaInicialControlador.personaSeleccionada.getTelefono());
        textFieldTfnoMovil.setText(PantallaInicialControlador.personaSeleccionada.getMovil());
        UtilJavaFx.setDateInDatePicker(datePickerFecha, PantallaInicialControlador.personaSeleccionada.getFecha());
        textFieldNombre.setEditable(false);
        textFieldApellidos.setEditable(false);
        textFieldSexo.setEditable(false);
        textFieldDni.setEditable(false);
        textFieldDireccion.setEditable(false);
        textFieldCP.setEditable(false);
        textFieldPoblacion.setEditable(false);
        textFieldTfnoFijo.setEditable(false);
        textFieldTfnoMovil.setEditable(false);
        datePickerFecha.setEditable(false);
    }

    public void mostrarDatosPersonaNueva() {
        textFieldNombre.setText(personaNueva.getNombre());
        textFieldApellidos.setText(personaNueva.getApellidos());
        textFieldSexo.setText(personaNueva.getSexo());
        textFieldDni.setText(personaNueva.getDni());
        textFieldDireccion.setText(personaNueva.getDireccion());
        textFieldCP.setText(personaNueva.getCodigoPostal());
        textFieldPoblacion.setText(personaNueva.getPoblacion());
        LOG.warning(personaNueva.getPoblacion());
        textFieldTfnoFijo.setText(personaNueva.getTelefono());
        textFieldTfnoMovil.setText(personaNueva.getMovil());
        UtilJavaFx.setDateInDatePicker(datePickerFecha, personaNueva.getFecha());
        LOG.fine("NuevaPersona");
    }

    @FXML
    private void onMouseClickedButtonModified(MouseEvent event) {
        textFieldNombre.setEditable(true);
        textFieldApellidos.setEditable(true);
        textFieldSexo.setEditable(true);
        textFieldDni.setEditable(true);
        textFieldDireccion.setEditable(true);
        textFieldCP.setEditable(true);
        textFieldPoblacion.setEditable(true);
        textFieldTfnoFijo.setEditable(true);
        textFieldTfnoMovil.setEditable(true);
        datePickerFecha.setEditable(true);
    }

    @FXML
    private void onMouseClickedButtonSave(MouseEvent event) throws JAXBException {
        try {
            // Crear una conexión con la URL del Servlet
            String strConnection = "http://localhost:8084/AgendaWeb/AgendaServlet";
            URL url = new URL(strConnection);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            // La conexión se va a realizar para poder enviar y recibir información
            // en formato XML
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "text/xml");

            switch (tipoPeticion) {
                case PETICION_POST: //nuevo
                    LOG.fine("Peticion Post");
                    conn.setRequestMethod("POST");
                    //Recogemos los datos de la nueva persona introducida
                    personaNueva.setNombre(textFieldNombre.getText());
                    personaNueva.setApellidos(textFieldApellidos.getText());
                    personaNueva.setSexo(textFieldSexo.getText());
                    personaNueva.setDni(textFieldDni.getText());
                    personaNueva.setDireccion(textFieldDireccion.getText());
                    personaNueva.setCodigoPostal(textFieldCP.getText());
                    personaNueva.setPoblacion(textFieldPoblacion.getText());
                    personaNueva.setTelefono(textFieldTfnoFijo.getText());
                    personaNueva.setMovil(textFieldTfnoMovil.getText());
                    personaNueva.setFecha(UtilJavaFx.getDateFromDatePicket(datePickerFecha));
                    LOG.fine("Fecha Persona Nueva: " + personaNueva.getFecha());

                    //Añadimos a la persona a la ultima fila de la tabla 
                    int ultimaPersona = tableView.getItems().size() - 1;
                    tableView.getItems().set(ultimaPersona, personaNueva);
                    tableView.getSelectionModel().select(ultimaPersona);
                    LOG.fine("Nombre: " + personaNueva.getNombre());
                    LOG.fine("ÍndicePersonaSeleccionada: " + ultimaPersona);

                    JAXBContext jaxbContext = JAXBContext.newInstance(Persona.class);
                    // Enviar al servidor, en formato XML, el objeto que se ha pasado por parámetro
                    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                    jaxbMarshaller.marshal(personaNueva, conn.getOutputStream());
                    // Ejecutar la conexión y obtener la respuesta
                    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                    isr.close();
                    break;
                case PETICION_PUT:  //editar
                    LOG.fine("Peticion Put");
                    PantallaInicialControlador.personaSeleccionada.setNombre(textFieldNombre.getText());
                    PantallaInicialControlador.personaSeleccionada.setApellidos(textFieldApellidos.getText());
                    PantallaInicialControlador.personaSeleccionada.setSexo(textFieldSexo.getText());
                    PantallaInicialControlador.personaSeleccionada.setDni(textFieldDni.getText());
                    PantallaInicialControlador.personaSeleccionada.setDireccion(textFieldDireccion.getText());
                    PantallaInicialControlador.personaSeleccionada.setCodigoPostal(textFieldCP.getText());
                    PantallaInicialControlador.personaSeleccionada.setPoblacion(textFieldPoblacion.getText());
                    PantallaInicialControlador.personaSeleccionada.setTelefono(textFieldTfnoFijo.getText());
                    PantallaInicialControlador.personaSeleccionada.setMovil(textFieldTfnoMovil.getText());
                    PantallaInicialControlador.personaSeleccionada.setFecha(UtilJavaFx.getDateFromDatePicket(datePickerFecha));
                    int indicePersonaSeleccionada = tableView.getSelectionModel().getSelectedIndex();
                    tableView.getItems().set(indicePersonaSeleccionada, PantallaInicialControlador.personaSeleccionada);
                    conn.setRequestMethod("PUT");

                    JAXBContext jaxbContext1 = JAXBContext.newInstance(Persona.class);
                    // Enviar al servidor, en formato XML, el objeto que se ha pasado por parámetro
                    Marshaller jaxbMarshaller1 = jaxbContext1.createMarshaller();
                    jaxbMarshaller1.marshal(PantallaInicialControlador.personaSeleccionada, conn.getOutputStream());
                    // Ejecutar la conexión y obtener la respuesta
                    InputStreamReader isr1 = new InputStreamReader(conn.getInputStream());
                    isr1.close();
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
        cerrarVentana();
    }

    @FXML
    private void onMouseClickedButtonDelete(MouseEvent event) throws JAXBException {
        Alert alert = new Alert(AlertType.CONFIRMATION, "¿Estas seguro que deseas eliminar a la persona?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            //Borramos la persona de la lista gráfica
            PantallaInicialControlador.listaObservablePersonas.remove(PantallaInicialControlador.personaSeleccionada);

            //Borramos la persona contenida en la lista en el servidor
            JAXBContext jaxbContext = JAXBContext.newInstance(Persona.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            try {
                // Crear una conexión con la URL del Servlet
                String strConnection = "http://localhost:8084/AgendaWeb/AgendaServlet";
                URL url = new URL(strConnection);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                // La conexión se va a realizar para poder enviar y recibir información
                // en formato XML
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-type", "text/xml");
                conn.setRequestMethod("DELETE");
                jaxbMarshaller.marshal(PantallaInicialControlador.personaSeleccionada, conn.getOutputStream());
                //Ejecutar la conexión y obtener la respuesta
                InputStreamReader isr1 = new InputStreamReader(conn.getInputStream());
                LOG.fine("Peticion Delete: se ha borrado la persona");
                isr1.close();
            } catch (IOException ex) {
                Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
            }
            LOG.fine("Presionado el botón Aceptar");
            cerrarVentana();
        } else {
            LOG.fine("Presionado el botón Cancelar");
        }
    }

    @FXML
    private void onMouseClickedButtonClose(MouseEvent event) {
        cerrarVentana();
    }

    private void cerrarVentana() {
        int lastScreensNumber = Agenda.root.getChildren().size() - 1;
        Agenda.root.getChildren().remove(lastScreensNumber);
        LOG.finest("Se ha cerrado la ventana");
    }
}